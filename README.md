# **microG systemless**
## Description
This module will let you install the latest version of microG systemless-ly, together with a couple of location providers.
## Changelog
No changelog yet
## Requirements
- latest Magisk version
## Instructions
1. Install module via Magisk Manager
2. Restart
3. Enable microG through its settings app
4. Restart again, check functionality
## Links

[Latest stable Magisk](http://www.tiny.cc/latestmagisk)
